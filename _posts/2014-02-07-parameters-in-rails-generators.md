---
layout: post
cover: false
title: Boolean parameters in rails generators
date:   2016-03-24 15:32:14 -0300
tags: rails generators
subclass: 'post tag-fiction'
categories: rails ror generators
---

~~~ ruby
class_option :stylesheet, :type => :boolean, :default => true, :description => "Include stylesheet file"
~~~

so you can pass `--stylesheet` on command line.

to check this value on your generator class, use `options.stylesheet?`

if you want to use something like `--two-words`, do:

~~~ ruby
class_option :'no-coffeescript', :type => :boolean, :default => false, :desc => 'Skips coffeescript replacement into app generators'
~~~

and to check

~~~ruby
options[:'no-coffeescript']
~~~

from http://railscasts.com/episodes/218-making-generators-in-rails-3?view=asciicast
